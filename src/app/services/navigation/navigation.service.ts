import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

interface IMenuItem {
  type: string;      // Possible values: link/dropDown/icon/separator/extLink
  name?: string;      // Used as display text for item and title for separator type
  state?: string;     // Router state
  icon?: string;      // Item icon name
  tooltip?: string;   // Tooltip text
  disabled?: boolean; // If true, item will not be appeared in sidenav.
  sub?: IChildItem[]; // Dropdown items
}
interface IChildItem {
  name: string;       // Display text
  state: string;
  childState: string;  // ChildRouter state
}

@Injectable()
export class NavigationService {
  constructor() {}

  defaultMenu: IMenuItem[] = [
    {
      name: 'HOME',
      type: 'link',
      tooltip: 'Home',
      icon: 'home',
      state: 'home'
    },
    {
      name: 'LATEST NEWS',
      type: 'link',
      tooltip: 'Latest New',
      icon: 'notifications',
      state: 'latest'
    },
    {
      name: 'ORGANISATION',
      type: 'link',
      tooltip: 'Organisation',
      icon: 'account_balance',
      state: 'org'
    },
    {
      name: 'CHANNELS & DRAINS',
      type: 'dropDown',
      tooltip: 'Channels & Drains',
      icon: 'all_inclusive',
      state: 'channels',
      sub: [
        {name: 'BWS', state: 'bws', childState: 'link'},
        {name: 'YWS NORTH', state: 'ywsnorth', childState: 'link'},
        {name: 'YWS SOUTH', state: 'ywssouth', childState: 'link'},
        {name: 'LCU', state: 'lcu', childState: 'link'},
      ]
    },
    {
      name: 'E-TENDERS',
      type: 'extLink',
      tooltip: 'E.Tenders',
      icon: 'event',
      state: 'https://etenders.hry.nic.in'
    },
    {
      name: 'TENDERS-LIST',
      type: 'extLink',
      tooltip: 'Tenders List',
      icon: 'event_note',
      state: 'http://tenders.gov.in/department.asp?id=357'
    },

    {
      name: 'INFORMATION SYSTEMS',
      type: 'dropDown',
      tooltip: 'Information Systems',
      icon: 'laptop_mac',
      state: 'channels',
      sub: [
        {name: 'FILE MOVEMENT & TRACKING', state: 'http://web1.hry.nic.in/cfmshcs', childState: 'extLink'},
        {name: 'BIOMETRIC ATTENDANCE', state: 'http://hrirrigation.attendance.gov.in/', childState: 'extLink'},
        {name: 'BUDGET ALLOCATION', state: 'http://web1.hry.nic.in/ba', childState: 'extLink'},
        {name: 'WORKS MONITORING', state: 'http://web1.hry.nic.in/pwdhid', childState: 'extLink'},
        {name: 'COURT CASE MONITORING', state: 'http://web1.hry.nic.in/courtcases', childState: 'extLink'},
        {name: 'DISCIPLINARY CASE MONITORING', state: 'http://web1.hry.nic.in/disciplinarycases', childState: 'extLink'},
        {name: 'H.R.M.S', state: 'http://164.100.137.158/pis/', childState: 'extLink'},
        {name: 'ASSET MANAGEMENT APPLICATION', state: 'http://202.164.49.101:8090/', childState: 'extLink'},
        {name: 'E-FILING FOR HIGH COURT', state: 'https://phhc.gov.in/ediary/', childState: 'extLink'},
      ]
    },
    {
      name: 'RTI',
      type: 'link',
      tooltip: 'Right To Information',
      icon: 'info',
      state: 'rti'
    },
    {
      name: 'DOCUMENTS',
      type: 'link',
      tooltip: 'Important Documents',
      icon: 'library_books',
      state: 'docs'
    },
    {
      name: 'HIRMI',
      type: 'extLink',
      tooltip: 'Hirmi',
      icon: 'domain',
      state: 'http://www.kurukshetra.nic.in/Organisations/HIRMI/hirmi.htm'
    },
  ]
  iconTypeMenuTitle = 'Frequently Accessed';
  menuItems = new BehaviorSubject<IMenuItem[]>(this.defaultMenu);
  menuItems$ = this.menuItems.asObservable();
}
