import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LcuComponent } from './lcu.component';

describe('LcuComponent', () => {
  let component: LcuComponent;
  let fixture: ComponentFixture<LcuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LcuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LcuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
