import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YwsnorthComponent } from './ywsnorth.component';

describe('YwsnorthComponent', () => {
  let component: YwsnorthComponent;
  let fixture: ComponentFixture<YwsnorthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YwsnorthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YwsnorthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
