import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YwssouthComponent } from './ywssouth.component';

describe('YwssouthComponent', () => {
  let component: YwssouthComponent;
  let fixture: ComponentFixture<YwssouthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YwssouthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YwssouthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
