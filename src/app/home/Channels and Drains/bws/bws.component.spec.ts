import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BwsComponent } from './bws.component';

describe('BwsComponent', () => {
  let component: BwsComponent;
  let fixture: ComponentFixture<BwsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BwsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BwsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
