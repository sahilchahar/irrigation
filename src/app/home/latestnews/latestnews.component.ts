import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-latestnews',
  templateUrl: './latestnews.component.html',
  styleUrls: ['./latestnews.component.css']
})
export class LatestnewsComponent implements OnInit {
  jeRecruitemnt = [{
    srNo: '1',
    message: 'JOINING LETTER DATED 12-10-2017 \n',
    date: '12-10-2017',
    pdf: '../assets/pdfs/JE/joiningletter12102017.PDF'
    },
    {
    srNo: '2',
    message: 'CORRIGENDUM DATED 18-09-2017',
    date: '18-09-2017',
    pdf: '../assets/pdfs/JE/Corrigendum18092017.pdf'
  },
    {
      srNo: '3',
      message: 'NOTICE DATED 12-09-2017',
      date: '12-09-2017',
      pdf: '../assets/pdfs/JE/Notice12092017.pdf'
    },
    {
      srNo: '4',
      message: 'Joining Letter Dated 23-08-2017 (Note:-Last Date of Joining is Extended Upto 18-09-2017)',
      date: '18-09-2017',
      pdf: '../assets/pdfs/JE/joining23082017.pdf'
    },
    {
      srNo: '5',
      message: 'List of Shortlisted Candidates After Physical Verfication of Documents on Dated 10-08-2017',
      date: '10-08-2017',
      pdf: '../assets/pdfs/JE/List-22082017.pdf'
    },
    {
      srNo: '6',
      message: 'Selection Criteria',
      date: '--/--/----',
      pdf: '../assets/pdfs/JE/selection_criteria.pdf'
    },
    {
      srNo: '7',
      message: 'Corrigendum Dated 09-08-2017',
      date: '09-08-2017',
      pdf: '../assets/pdfs/JE/NOTICE 090817.pdf'
    },
    {
      srNo: '8',
      message: 'Notice Dated 01-08-2017',
      date: '01-08-2017',
      pdf: '../assets/pdfs/JE/Notice01082017.pdf'
    }
  ]
  constructor() { }

  ngOnInit() {
  }
}
