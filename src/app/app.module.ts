import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TopbarComponent } from './home/topbar/topbar.component';
import { SidebarComponent } from './home/sidebar/sidebar.component';
import {SharedMaterialModule} from './helpers/shared-material.module';
import {ThemeService} from "./services/theme/theme.service";
import {AppRoutingModule} from "./app-routing.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {NavigationService} from "./services/navigation/navigation.service";
import {CommonDirectivesModule} from "./directives/common/common-directives.module";
import { HomeComponent } from './home/home.component';
import { OrganisationComponent } from './home/organisation/organisation.component';
import { BwsComponent } from './home/Channels and Drains/bws/bws.component';
import { YwsnorthComponent } from './home/Channels and Drains/ywsnorth/ywsnorth.component';
import { YwssouthComponent } from './home/Channels and Drains/ywssouth/ywssouth.component';
import { LcuComponent } from './home/Channels and Drains/lcu/lcu.component';
import { RtiComponent } from './home/rti/rti.component';
import { DocsComponent } from './home/docs/docs.component';
import { LatestnewsComponent } from './home/latestnews/latestnews.component';
import {FlexLayoutModule} from "@angular/flex-layout";
import { JeResultComponent } from './je-result/je-result.component';


@NgModule({
  declarations: [
    AppComponent,
    TopbarComponent,
    SidebarComponent,
    HomeComponent,
    OrganisationComponent,
    BwsComponent,
    YwsnorthComponent,
    YwssouthComponent,
    LcuComponent,
    RtiComponent,
    DocsComponent,
    LatestnewsComponent,
    JeResultComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedMaterialModule,
    AppRoutingModule,
    CommonDirectivesModule,
    FlexLayoutModule
  ],
  providers: [ThemeService, NavigationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
