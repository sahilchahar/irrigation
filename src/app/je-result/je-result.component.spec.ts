import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JeResultComponent } from './je-result.component';

describe('JeResultComponent', () => {
  let component: JeResultComponent;
  let fixture: ComponentFixture<JeResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JeResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JeResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
