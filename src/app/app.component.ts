import {Component, OnInit, ViewChild} from '@angular/core';
import {Subscription} from "rxjs/Subscription";
import {NavigationEnd, Router} from "@angular/router";
import { MatSidenav } from '@angular/material';
import {ThemeService} from "./services/theme/theme.service";
import * as Ps from 'perfect-scrollbar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'app';
  private isMobile;
  screenSizeWatcher: Subscription;
  isSidenavOpen: Boolean = false;
  url;
  @ViewChild(MatSidenav) private sideNave: MatSidenav;

  constructor(
    private router: Router,
    public themeService: ThemeService
  ) {
    // Close sidenav after route change in mobile
  /*  router.events.filter(event => event instanceof NavigationEnd).subscribe((routeChange: NavigationEnd) => {
      this.url = routeChange.url;
      if (this.isNavOver()) {
        this.sideNave.close();
      }
    });*/
  }
  ngOnInit() {
    // Initialize Perfect scrollbar for sidenav
    let navigationHold = document.getElementById('scroll-area');
    Ps.initialize(navigationHold, {
      suppressScrollX: true
    });
    console.log(this.themeService.activatedThemeName);
  }
  isNavOver() {
    return window.matchMedia(`(max-width: 960px)`).matches;
  }
}
