import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {OrganisationComponent} from './home/organisation/organisation.component';
import {BwsComponent} from './home/Channels and Drains/bws/bws.component';
import {YwsnorthComponent} from './home/Channels and Drains/ywsnorth/ywsnorth.component';
import {YwssouthComponent} from './home/Channels and Drains/ywssouth/ywssouth.component';
import {LcuComponent} from './home/Channels and Drains/lcu/lcu.component';
import {RtiComponent} from './home/rti/rti.component';
import {DocsComponent} from './home/docs/docs.component';
import {LatestnewsComponent} from "./home/latestnews/latestnews.component";
import {JeResultComponent} from "./je-result/je-result.component";

const appRoutes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'org',
    component: OrganisationComponent
  },
  {
    path: 'channels/bws',
    component: BwsComponent
  },
  {
    path: 'channels/ywsnorth',
    component: YwsnorthComponent
  },
  {
    path: 'channels/ywssouth',
    component: YwssouthComponent
  },
  {
    path: 'channels/lcu',
    component: LcuComponent
  },
  {
    path: 'rti',
    component: RtiComponent
  },
  {
    path: 'docs',
    component: DocsComponent
  }
  ,
  {
    path: 'latest',
    component: LatestnewsComponent
  },
  {
    path: 'jeResult',
    component: JeResultComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
